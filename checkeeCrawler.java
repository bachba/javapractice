import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;


public class checkeeCrawler {

	public static LinkedList<String> links;
	public static String host;
	public static int rate;
	public static int pages;
	public static int bytes;
	public static ArrayList<String> files;
	public static void main(String args[]) throws Exception{
		//get the target host
		host = "www.checkee.info";
		//String dir = "/main.php?dispdate=2014-12";
		rate = 1;
		//create the folder to store the html files
		new File(host+"/").mkdir();
		//an arraylist to store the already visited pages
		files = new ArrayList<String>();
		//this is the queue to tempararily store new links 
		links = new LinkedList<String>();
		crawl_index();
		long last = System.currentTimeMillis();
		//byte / sec
		if(rate<20){
			while(links.size()>0){
				long current = System.currentTimeMillis();
				if(current-last>1000){
					bytes -= rate;
					if(bytes<0)
						bytes = 0;
					last = current;
				}
				if(links.size()!=0 && bytes<rate){
					String link = links.poll();
					String result = request(link);
				}
			}
		}
		

	}
	
	public static void crawl_index() throws Exception{
		
		Socket s = null;
		PrintWriter out = null;
		BufferedReader in = null;
		//TCP connecto to the server on port 80
		try {
			s = new Socket(host, 80);
			out = new PrintWriter(s.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Don't know about host");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection");
			System.exit(1);
		}
		//HTTP request
		System.out.println("Sending requst to "+host+"/index.php");
		out.println("GET "+"/index.php"+" HTTP/1.1");
		out.println("Host: "+host);
		out.println("Connection: close");
		out.println();
		//get the response status
		String temp = in.readLine();
		System.out.println(temp);
		if(!temp.equals("HTTP/1.1 200 OK")){
			System.out.println("Failed");
			System.out.println();
			return;
		}
		//update number of pages
		pages++;
		System.out.println("page number: "+pages);
		//get the headers
		String extension = "";
		while((temp=in.readLine())!=null){
			if(temp.length()==0){
				break;
			}
			//System.out.println(temp.length());
			String[] tokens = temp.split(": ");
			if(tokens[0].equals("Content-Type")){
				System.out.println(temp);
				extension = "."+tokens[1].split("/")[1].split(";")[0];
			}
			else if(tokens[0].equals("Content-Length")){
				System.out.println(temp);
				bytes += Integer.parseInt(tokens[1]);
				System.out.println("Bytes number: "+bytes);
			}
		}
		//contruct the filename
		
		//get the file content
		StringBuffer stream = new StringBuffer();
		while((temp=in.readLine())!=null){
			stream.append(temp);
		}
		int curr = 0;
		for (int i=0;i<3;i++){
			curr = stream.indexOf("href",curr+1);
		}
		while (curr<stream.lastIndexOf("href")){
			curr = stream.indexOf("href",curr+1);
			int start = stream.indexOf("/", curr);
			int end = stream.indexOf("\"", start);
			links.offer(stream.substring(start, end));
		}
		//save to local
		s.close();
		in.close();
		out.close();
	}
	
	public static String request(String dir) throws Exception{
		//update the list of visited files
		if(files.contains(dir))
			return "";
		else
			files.add(dir);
		Socket s = null;
		PrintWriter out = null;
		BufferedReader in = null;
		//TCP connecto to the server on port 80
		try {
			s = new Socket(host, 80);
			out = new PrintWriter(s.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Don't know about host");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection");
			System.exit(1);
		}
		//HTTP request
		System.out.println("Sending requst to "+host+dir);
		out.println("GET "+dir+" HTTP/1.1");
		out.println("Host: "+host);
		out.println("Connection: close");
		out.println();
		//get the response status
		String temp = in.readLine();
		System.out.println(temp);
		if(!temp.equals("HTTP/1.1 200 OK")){
			System.out.println("Failed");
			System.out.println();
			return "";
		}
		//update number of pages
		pages++;
		System.out.println("page number: "+pages);
		//get the headers
		String extension = "";
		while((temp=in.readLine())!=null){
			if(temp.length()==0){
				break;
			}
			//System.out.println(temp.length());
			String[] tokens = temp.split(": ");
			if(tokens[0].equals("Content-Type")){
				System.out.println(temp);
				extension = "."+tokens[1].split("/")[1].split(";")[0];
			}
			else if(tokens[0].equals("Content-Length")){
				System.out.println(temp);
				bytes += Integer.parseInt(tokens[1]);
				System.out.println("Bytes number: "+bytes);
			}
		}
		//contruct the filename
		//cases only have directory , without filename
		String filename = "";
		int equal = dir.lastIndexOf('=');
		
			filename = dir.substring(equal+1);
		
		if(!filename.endsWith(extension)){
			filename+=extension;
		}
		dir = host+"/";
		//get the file content
		StringBuffer stream = new StringBuffer();
		while((temp=in.readLine())!=null){
			//if(extension.startsWith(".html"))
				//temp = findLink(temp,dir);
			stream.append(temp);
		}
		//save to local
		process_data(stream);
		//save(stream.toString(),dir,filename);
		s.close();
		in.close();
		out.close();
		return dir+":::"+filename;
	}
	
	public static void process_data(StringBuffer s){
		int curr = 0;
		int next = 0;
		String id;
		String visa_type;
		String visa_entry;
		String city;
		String major;
		String status;
		String check_date;
		String complete_date;
		String waiting_days;
		curr = s.indexOf("Update", curr);
		//curr = s.indexOf("Update", curr+1);
		while (curr<s.length()&&s.indexOf("Update", curr+1)>0){
			curr = s.indexOf("Update", curr+1);
			curr = s.indexOf("<td>", curr);
			next = s.indexOf("</td>",curr);
			id = s.substring(curr+4, next);
			curr = s.indexOf("<td>", curr+1);
			next = s.indexOf("</td>",curr);
			visa_type = s.substring(curr+4, next);
			curr = s.indexOf("<td>", curr+1);
			next = s.indexOf("</td>",curr);
			visa_entry = s.substring(curr+4, next);
			curr = s.indexOf("<td>", curr+1);
			next = s.indexOf("</td>",curr);
			city = s.substring(curr+4, next);
			curr = s.indexOf("<td>", curr+1);
			next = s.indexOf("</td>",curr);
			major = s.substring(curr+4, next);
			curr = s.indexOf("<td>", curr+1);
			next = s.indexOf("</td>",curr);
			status = s.substring(curr+4, next);
			curr = s.indexOf("<td>", curr+1);
			next = s.indexOf("</td>",curr);
			check_date = s.substring(curr+4, next);
			curr = s.indexOf("<td>", curr+1);
			next = s.indexOf("</td>",curr);
			complete_date = s.substring(curr+4, next);
			curr = s.indexOf("<td>", curr+1);
			next = s.indexOf("</td>",curr);
			waiting_days = s.substring(curr+4, next);
			sql_save(id, visa_type, visa_entry, city, major, status, check_date, complete_date, waiting_days);
			System.out.print(id+", "+visa_type+", " +visa_entry+", "+city+", "+major+", "+status+", "+check_date+", "
                                                        + complete_date+ ", "+waiting_days);
		}
		
	}
	
	public static void sql_save(String id, String visa_type, String visa_entry, String city, String major, String status, 
			String check_date, String complete_date, String waiting_days){
		
		String dbUrl = "jdbc:mysql://localhost:3306/checkee?autoReconnect=true";
		String dbUser = "test";
		String dbPassword = "gemcloud";
		Connection dbcon = null;
		String query = "Insert INTO test(id,visa_type,visa_entry,city, major, status, check_date," +
				"complete_date, waiting_days) VALUES(?,?,?,?,?,?,?,?,?)";
		//Date date;
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
		try {
			dbcon = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
			PreparedStatement ps = dbcon.prepareStatement(query);
			ps.setString(1, id);
			ps.setString(2, visa_type);
			ps.setString(3, visa_entry);
			ps.setString(4, city);
			ps.setString(5, major);
			ps.setString(6, status);
			ps.setString(7,check_date);
			ps.setString(8,complete_date);
			ps.setInt(9,Integer.parseInt(waiting_days));
			ps.executeUpdate();
		} catch (SQLException e) {
			System.out.print(e.getMessage());
		} 
		
		
	}
	
	public static void save(String s, String dir, String filename) throws Exception{
		try{
			System.out.println("Saving "+filename+" to "+dir+"....");
			File d = new File(dir);
			if(!d.exists())
				d.mkdirs();
			File file = new File(dir+filename);
			BufferedWriter output = new BufferedWriter(new FileWriter(file));
			output.write(s);
			output.close();
			System.out.println("Success!");
			System.out.println();
			return;
		}
		catch(IOException e){
			System.out.println("writing file error");
			return;
		}
	}
}
