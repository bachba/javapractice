import java.util.ArrayList;
import java.util.List;
import java.util.Stack;


public class uniqueBST2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		generateTrees(8);//wrong approach, should do left subtree right subtree recursively then combine
	}
	
	
	
	 
	
	    public static List<TreeNode> generateTrees(int n) {
	        List<TreeNode> results = new ArrayList<TreeNode>();
	        if (n==0) return results;
	        List<Integer> numbers = new ArrayList<Integer>();//to track the rest of numbers can be inserted to the tree
	        for (int i=1;i<=n;i++){
	            numbers.add(i);
	        }
	        Stack<Integer> used = new Stack<Integer>();
	        generateTrees((ArrayList)results, (ArrayList) numbers, used);
	        return results;
	    }
	    
	    public static void generateTrees(ArrayList<TreeNode> r, ArrayList<Integer> numbers, Stack<Integer> used){
	        if (numbers.size()==0){
	            r.add(generateBST(used));
	            return;
	        }
	        
	        for (int i=0;i<numbers.size();i++){
	            
	            used.push(numbers.get(i));//get the number used in this case
	            numbers.remove(i);
	            
	            generateTrees(r,numbers,used);
	            
	            numbers.add(i,used.pop());//put the number back
	        }
	        return;
	        
	    }
	    
	    public static TreeNode generateBST(Stack<Integer> numbers){
	        Stack<Integer> copy = (Stack<Integer>)numbers.clone();
	        TreeNode head = new TreeNode(copy.pop());
	        
	        while (!copy.isEmpty()){
	            TreeNode node = head;
	            TreeNode temp = new TreeNode(copy.pop());
	            while(true){
	                if (temp.val <node.val){
	                    if (node.left!=null){
	                        node = node.left;
	                    }else{
	                        node.left = temp;
	                        break;
	                    }
	                }else{
	                    if (node.right!=null){
	                        node = node.right;
	                    }else{
	                        node.right = temp;
	                        break;
	                    }
	                }
	            }
	        }
	        return head;
	    }
	
	    static class TreeNode {
		      int val;
		      TreeNode left;
		      TreeNode right;
		      TreeNode(int x) { val = x; left = null; right = null; }
		  }
}

