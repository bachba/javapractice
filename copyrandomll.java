import java.util.ArrayList;


public class copyrandomll {
	public static RandomListNode copyRandomList(RandomListNode head) {
        if (head==null){return head;}
        RandomListNode headcopy = new RandomListNode(head.label);
        if (head.next ==null){
        	if (head.random==null){
            return headcopy;
        	}else{
        		headcopy.random = headcopy;
        		return headcopy;
        	}
        }
        
        RandomListNode temp=head;
        RandomListNode prev=headcopy;
        // ArrayList<Integer> ar = new ArrayList<Integer>();
        // ar.add(temp.label);
        // while(temp.next!=null){
        //     ar.add(temp.label);
        // }
        // int index = ar.size();
        // headcopy.val = 
        // while(index>=0){
            
        // }
        int size =0;
        while(temp.next!=null){
            prev.next = new RandomListNode(temp.next.label);//copy the next node in the ll
            prev = prev.next;
            temp = temp.next;
            size++;
        }
        //now headcopy is a copy of original linkedlist without random pointers
        
        RandomListNode currentcopy = headcopy;
        temp = head;
        while(currentcopy.next!=null){
            RandomListNode runner1 = headcopy;
            //O(n)
            if (temp.random!=null){
            RandomListNode runner2 = temp.random;
            int index = 0;
            while(runner2.next!=null){//stop when runner2 hit the last node
                index++;
                runner2 = runner2.next;
            }
            for (int i=0;i<size-index;i++){
                runner1 = runner1.next;
            }
            currentcopy.random = runner1;
            }else{
                currentcopy.random =null;
            }
            currentcopy = currentcopy.next;
            temp = temp.next;
        }
        return headcopy;
    }
	public static void main(String args[]) {
    	RandomListNode head = new RandomListNode(0);
    	RandomListNode prev = head;
    	for (int i=1;i<2;i++){
    		RandomListNode tail = new RandomListNode(i);
    		prev.next = tail;
    		prev.random = tail;
    		prev = tail;
    	}
    	RandomListNode copy = copyRandomList(head);
    	System.out.println(copy.label);
    	while (copy.next!=null){
    		copy = copy.next;
    		System.out.println(copy.label);
    	}
    	int[] a = new int[10];
    	ArrayList<ArrayList<Integer>> test = new ArrayList<ArrayList<Integer>>();
    	
    }
}

class RandomListNode {
	   int label;
	   RandomListNode next,random;
	   RandomListNode(int x) { this.label = x; next = null;}
	}
