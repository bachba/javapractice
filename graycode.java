import java.util.ArrayList;


public class graycode {
	//get n-bit gray code
	public static ArrayList<Integer> grayCode(int n) {
		
        ArrayList<Integer> gc = new ArrayList<Integer>();
        gc.add(0);
        // gc.add(1);
        for (int i=0;i<n;i++){
            int inc = 1<<i;
            int m=gc.size();
            for (int j=0;j<m;j++){
                gc.add(gc.get(m-j-1)+inc);
            }
        }
        return gc;
    }
	public static void main(String[] args) {
		ArrayList<Integer> gc = grayCode(8);
		int n =4;
		//System.out.println(1<<31);
		//System.out.println(n);
		System.out.println(getNthGrayCode(n+1));
		System.out.println(gc.get(n));
		System.out.println(revGray(getNthGrayCode(n+1)));
		System.out.println(Int2Binary(getNthGrayCode(n+1)));
		//System.out.println(Integer.toBinaryString(5));
		
	}
	
	static int getNthGrayCode(int n){
		n--;
		return n^(n>>1); 
	}
	
	static int revGray(int n){
		int result = (1<<31)&n;
		
		/* alternative to the below algorithm		
		for (int i = 31;i>0;i--){
			result += (((1<<i)&result)>>1) ^((1<<(i-1))&n);
		}*/
		
		for (int mask = 1<<30;mask>0;){
			result += ((mask&result)>>1)^((mask>>1)&n);
			mask=mask>>1;
		}
		++result;
		/*
		//only work for unsigned int
		for (int mask = n>>1;mask>0;mask=mask>>1){
			//result += ((mask&result)>>1)^((mask>>1)&n);
			result = result ^ mask;
		}
		*/
		return result;
	}
	
	
	static String Int2Binary(int n){
		String result = "";
		for (int i = 31;i>=0;i--){
			result += Integer.toString(((1<<i)&n)>>i);
		}	
		return result;
	}

}
