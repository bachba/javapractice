import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class CountingSort {
	public static int[] counting (int[] ar){
		int[] count = new int[100];
		for(int i=0;i<ar.length;i++){
			count[ar[i]]+=1;
		}
		return count;
	}

	public static int[] lcounting(int[] count){
		int temp=0;
		int total=0;
		for(int i = 0; i<count.length; i++){
			temp = count[i];
			count[i]=total;
			total +=temp;
		}
		return count;
	}

	public static String[] sorting(int[] count,int n,int[] ar,String[] strar){
		String[] results = new String[n];

		for(int i = 0; i<ar.length/2; i++){
			results[count[ar[i]]]="-";
			count[ar[i]]++;
		}
		for(int i = ar.length/2; i<ar.length; i++){
			results[count[ar[i]]]=strar[i];
			count[ar[i]]++;
		}
		return results;
	}

	static void printArray(String[] ar) throws IOException {
		OutputStream out = new BufferedOutputStream(System.out);
		for(String n: ar){
			out.write((n+" ").getBytes());
			//System.out.print(n+" ");
		}
		out.flush();
//		System.out.println("");
//		StringBuilder sb = new StringBuilder();
		 
	}
	public static void main(String[] args) throws Exception, IOException {
		BufferedReader in =  new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(in.readLine()); 
		int[] ar = new int[n];
		String[] st = new String[n];
		for(int i=0;i<n;i++){
			String[] input;
			input = in.readLine().split(" ");
				ar[i]=Integer.parseInt(input[0]); 
				st[i]=input[1];
			
		}
		int[] countresult = new int[100];
		countresult = counting(ar);
		int[] lcountresult = lcounting(countresult);
		String[] sortresult = sorting(lcountresult, n, ar,st);
		printArray(sortresult);
	}
}