
public class minStack {

	static class MinStack {
	    class Node{
	        int val;
	        Node next;
	        Node(int v){
	            val = v;
	        }
	    }
	    Node head;
	    Node tail;
	    Node min;
	    
	    public void push(int x) {
	        Node node = new Node(x);
	        if (head==null){ //first item in stack
	            head = node;
	            tail = node;
	            min = node;
	        }else{
	            tail.next = node;
	            tail = node;
	        }
	        
	        if (x<min.val) min = node;
	    }

	    public void pop() {
	        if (head==min){//find new min
	            Node node = head.next;
	            min = node;
	            while(min!=null&&node.next!=null){
	                min = min.val<node.next.val? min:node.next;
	                node = node.next;
	            }
	        }
	        if (head!=null) head = head.next; //after pop, head may become null
	        if (head==null) tail = null; //stack is empty now
	        
	    }

	    public int top() {
	    	System.out.println(head.val);
	        return head.val;
	    }

	    public int getMin() {
	    	System.out.println(min.val);
	        return min.val;
	    }
	}
	public static void main(String[] args) {
		MinStack m = new MinStack();
		m.push(2147483646);
		m.push(2147483646);
		m.push(2147483647);
		m.top();
		m.pop();
		m.getMin();
		m.pop();
		m.getMin();
		m.pop();
		m.push(2147483647);
		m.top();
		m.getMin();
		m.push(-2147483648);
		m.top();
		m.getMin();
		m.pop();
		m.getMin();

	}

}
