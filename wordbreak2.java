import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;


public class wordbreak2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s = "aaaaaaaaaaaaaaaaaaaaaaab";
		Set<String> d =new HashSet<String>(Arrays.asList("a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaab"));
		Long start = System.currentTimeMillis();
		List<String> r =wordBreak(s,d);
		System.out.print(System.currentTimeMillis()-start);
	}
	public static List<String> wordBreak(String s, Set<String> dict) {
        int end = s.length();
        int end2 = end-1;
        String endWord = new String();
        while(end2>=0){
        	if (!dict.contains(s.substring(end2))){
        		end2--;
        	}else{
        		endWord = s.substring(end2);
        		break;
        	}
        }
        
        Map<Integer,List<String>> hm = new HashMap<Integer,List<String>>();//hash map to store posiblle words starting from each "key"
        hm.put(end2-1,new ArrayList<String>());
        hm.put(-1,new ArrayList<String>());
        for (int i = 0; i < end2; i++){
            for (int j = 0; j <= i; j++){
                String sub = s.substring(j,i+1);
                if (dict.contains(sub)){
                    if (hm.containsKey(j-1)){
                        List<String> l = new ArrayList<String>();
                        if (j==0){
                            l.add(sub);
                        }else{
                            for (String word: hm.get(j-1)){
                                l.add(word+" "+sub);
                            }
                        }
                        if (hm.containsKey(i)){
                            l.addAll(hm.get(i));
                            hm.put(i,l);
                        }else{
                            hm.put(i,l);
                        }
                    }
                }
            }
        }
        List<String> results = new ArrayList<String>();
        for (String word:hm.get(end2-1)){
        	results.add(word+" "+endWord);
        }
        
        return results;
    }
	
	public static List<String> wordBreak2(String s, Set<String> dict) {
        
        List<String> results = new ArrayList<String>();
        String result = new String();
        recur(s,dict,result,results);
        return results;
    }
    public static void recur (String s, Set<String> dict, String result, List<String> results){
        int end = s.length();
        for (int j = 0;j< end;j++){
            String sub = s.substring(0,j);
            if (dict.contains(sub)){
                if (j==end-1){
                    results.add(result+" "+sub);
                }else{
                    recur(s.substring(j+1),dict,result+" "+sub,results);
                }
            }
        }
    }
    
    public static List<String> wordBreak3(String s, Set<String> dict) {
        Map<Integer, List<String>> validMap = new HashMap<Integer, List<String>>();

        // initialize the valid values
        List<String> l = new ArrayList<String>();
        l.add("");
        validMap.put(s.length(), l);

        // generate solutions from the end
        for(int i = s.length() - 1; i >= 0; i--) {
            List<String> values = new ArrayList<String>();
            for(int j = i + 1; j <= s.length(); j++) {
                if (dict.contains(s.substring(i, j))) {
                    for(String word : validMap.get(j)) {
                        values.add(s.substring(i, j) + (word.isEmpty() ? "" : " ") + word);
                    }
                }
            }
            validMap.put(i, values);
        }
        return validMap.get(0);
    }
}
