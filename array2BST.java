
public class array2BST {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] num = new int[]{-1, 0, 1, 2};
		
		convert(num,0,3);

	}

	public static TreeNode convert (int[] num, int left, int right){
        if (right-left<0) return null;
        int mid = left+(right-left)/2;
        TreeNode node = new TreeNode(num[mid]);
        if (right-left==0){
            return node;
        }
        node.left = convert(num,left, mid-1);
        node.right = convert(num,mid+1,right);
        return node;
    }
	public static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
}
