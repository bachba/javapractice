
public class longestPalindromicSubstring {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(longestPalindrome("illi"));
	}
	public static String longestPalindrome(String s) {
        if (s==null) return null;
        if (s.length()==0) return new String();
        if (s.length()==1) return s;
        
        int longest = 0;
        int start = 0;
        int end = 0;
        //if the palindromic substring is centered at an character
        for (int i = 1; i< s.length();i++){
            boolean flag = true;
            int curr =0;
            for (int j = 0; i-j>=0&&i+j<s.length();j++){
                if (s.charAt(i-j)!=s.charAt(i+j)) {
                    if (j*2-1>longest){
                        longest = j*2-1;
                        start = i-j+1;
                        end = i+j-1;
                    }
                    flag= false;
                    break;
                }
                curr++;
            }
            if (flag==true){
                if (curr*2-1>longest){
                        longest =curr *2-1;
                        start = i-curr+1;
                        end = i+curr-1;
                }
            }
        }
        
        //if the palindromic substring is centered inbetween of two characters
        for (int i = 1; i< s.length();i++){
            boolean flag = true;
            int curr =0;
            for (int j = 0; i-j>0&&i+j<s.length();j++){
                if (s.charAt(i-j-1)!=s.charAt(i+j)) {
                    if (j*2>longest){
                        longest = j*2;
                        start = i-j;
                        end = i+j-1;
                    }
                    flag= false;
                    break;
                }
                curr++;
            }
            if (flag==true){
                if (curr*2>longest){
                        longest =curr *2;
                        start = i-curr;
                        end = i+curr-1;
                }
            }
        }
        return s.substring(start,end+1);
    }
}
