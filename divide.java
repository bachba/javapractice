
public class divide {
	public static int divides(int dividend, int divisor) {
        if (dividend==Integer.MIN_VALUE&&divisor==Integer.MIN_VALUE){
        	return 1;
        }
        if (divisor==Integer.MIN_VALUE){
        	return 0;
        }
        
//        if(dividend==Integer.MIN_VALUE){
//            dividend=Integer.MIN_VALUE>>1;
//            return divides(dividend,divisor)<<1;
//        }
		long a = Math.abs((long)dividend);
        long b = Math.abs((long)divisor);
        long result=0;
//        while (a >= b) {
//            int c = b;
//            for (int i = 0; a >= c; ++i, c <<= 1) {
//                a -= c;
//                result += 1 << i;
//            }
//        }
        
        while(a>=b){
        	long tmp = b;
            int count =0;
        	while(a>=tmp<<1&&tmp<<1>0){
        		tmp = tmp<<1;
                count +=1;
        	}
//        	tmp = tmp>>1;
//        	count-=1;
            a-=tmp;
            result+=1<<count;
        }
        
        int sign;
        if (dividend>0&&divisor>0||dividend<0&&divisor<0){
        	sign = 1;
        }else{
        	sign = -1;
        }
        return (int) ((dividend^divisor)>>31<0 ? -result : result);
    }
	public static void main(String[] args) {
		int a = divides(-2147483648, -1109186033);
		System.out.println(a);

	}

}
