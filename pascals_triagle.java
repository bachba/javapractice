import java.util.ArrayList;
import java.util.List;


public class pascals_triagle {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(generate(5));
	}
	public static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> answer = new ArrayList<List<Integer>>();
        List<Integer> row = new ArrayList<Integer>();
        row.add(1);
        answer.add(row);
        for (int i=2;i<=numRows;i++){
            row = new ArrayList<Integer>(); 
            row.add(1);
            for (int j = 2;j<i;j++){
                row.add(answer.get(i-2).get(j-2)+answer.get(i-2).get(j-1));
            }
            row.add(1);
            answer.add(row);
        }
        return answer;
    }
}
