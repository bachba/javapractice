import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class tasktime {
	public static void main(String[] args)throws IOException{
		
		

		String[] splitted;
		String[] seperateraw;
		Map<String,String> resultsTMmap = new HashMap<String,String>(); 
//		List<Long> resultslist = new ArrayList<Long>();
//		List<Float> TMlist = new ArrayList<Float>();
		BufferedReader brTM=null;
		
		long total_time_processed=0;
		long total_time_processedTM04=0;
		int total_tasks=0;
		int total_tasks_TM04 = 0;
		int total_tasks_check = 0;
		double TMscoretotal = 0;
		double TMscore04 = 0;
		int missed = 0;
		double TMscoreMax = 0;
		double TMscoreMin = 1;
		
		
		long starttime = System.currentTimeMillis();
		try{
			brTM = new BufferedReader(new FileReader("C:\\Users\\He\\Dropbox\\UCB\\GEMcloud Data\\SAINT2Results\\TM_task_results_2.txt"));
			String rawdata=brTM.readLine();
			while(rawdata!=null){
				total_tasks++;

				seperateraw=rawdata.split("\\s+");
				rawdata = brTM.readLine();
				
				splitted = seperateraw[0].split("_");
//				resultslist.add(Long.parseLong(splitted[2]));
//				TMlist.add(Float.parseFloat(seperateraw[1]));
				resultsTMmap.put(splitted[2],seperateraw[1]);//key: task sent time, value: TMscore
			}
		}finally{
			brTM.close();
		}
		
		try{
			brTM = new BufferedReader(new FileReader("C:\\Users\\He\\Dropbox\\UCB\\GEMcloud Data\\SAINT2Results\\TM_task_results_2_filelist.txt"));
			String rawdata=brTM.readLine();
			while(rawdata!=null){

				seperateraw=rawdata.split("\\.");
				splitted = seperateraw[0].split("_");
				String senttime = splitted[2];
				long processingtime = Long.parseLong(splitted[3]);
				total_time_processed += processingtime; 
				if (resultsTMmap.get(senttime)==null){
					//System.out.println(senttime);
					missed++;
				}else{
					double tmscore = Double.parseDouble(resultsTMmap.get(senttime));
					if (tmscore > TMscoreMax) TMscoreMax = tmscore;
					if (tmscore < TMscoreMin) TMscoreMin = tmscore;
					if (tmscore>0.45){					
						TMscore04 += tmscore;
						total_time_processedTM04 +=processingtime;
						total_tasks_TM04++;
					}
					TMscoretotal += tmscore;
					resultsTMmap.remove(senttime);
					total_tasks_check++;
				}//get TMscore for the file
				
				rawdata = brTM.readLine();
			}
		}finally{
			brTM.close();
		}
				
		System.out.println("Time cost: "+(System.currentTimeMillis()-starttime));
		System.out.println("TMscoreMax: "+TMscoreMax+"TMscoreMin: "+TMscoreMin);
		System.out.println("Total TM045: "+total_tasks_TM04);
		System.out.println("TM04 ratio: "+(double)total_tasks_TM04/(double)total_tasks);
		System.out.println("Average time: "+total_time_processed/total_tasks);

		System.out.println("Average TMscore: "+TMscoretotal/total_tasks);
		System.out.println("Avarage time per TM045: "+total_time_processed/total_tasks_TM04);
		//including time processed for lower score results 
		System.out.println("Avarage score TM045: "+TMscore04/total_tasks_TM04);
			
	}
}
