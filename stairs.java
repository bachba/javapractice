import java.util.*;


public class stairs {

	static Map<Integer,Integer> cache = new HashMap<Integer,Integer>();
	public static int climbStairs(int n) {
		int total;
		if (n==0) return 0;
		if (n==1) return 1;
		if (n==2) return 2;
		//to speed up computation for large n, use a hashmap to store the number that has already been calculated
		if (cache.containsKey(n)){
			return cache.get(n);
		}
		total = climbStairs(n-1)+climbStairs(n-2);
		//store values that have not been discovered 
		cache.put(n,total);
		return total;
	}

	public static void main(String[] args) {
		int nStairs = 10;
		int total = climbStairs(nStairs);
		System.out.println("Total ways of reaching "+nStairs + " stairs: "+ total);
	}

}
