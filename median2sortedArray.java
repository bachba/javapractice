
public class median2sortedArray {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] a = new int[]{1,2};
		int[] b = new int[]{3,4,5,6};
		System.out.print(findMedianSortedArrays(a,b));

	}
	public static double findMedianSortedArrays(int A[], int B[]) {
        int a = A.length;
        int b = B.length;
        
        if (a==0){
        	return (double)b%2==1 ? B[(b-1)/2]:0.5*(B[(b-1)/2]+B[b/2]);
        }
        if (b==0){
        	return (double)a%2==1 ? A[(a-1)/2]:0.5*(A[(a-1)/2]+A[a/2]);
        }
        if (a==1){
        	if (b==1){
                return (double) 0.5*(A[0]+B[0]);
            }
        	if (b%2==1){
        		if (A[0]>B[(b-1)/2]) {
        	        return (double) 0.5*(B[(b-1)/2]+Math.min(A[0],B[(b+1)/2]));
        	    }else{
        	        return (double) 0.5*(B[(b-1)/2]+Math.max(A[0],B[(b-3)/2]));
        	    }
        	}else{
        		return (double) Math.min(Math.max(A[0],B[(b-1)/2]),B[b/2]);
        	}
        }
        if (b==1){
        	return findMedianSortedArrays(B, A);
        }
        
        
        int al = 0;
        int ar = a-1;
        int bl = 0;
        int br = b-1;
        int left = (a+b)%2==1 ? 1:2;
        
        
        while(ar-al>1 || br-bl>1){
            int midA = A[(ar+al)/2];
            int midB = B[(br+bl)/2];
            if (midA>=midB){
                ar = ((ar+al+1)/2);//if n is even, include the middle right one
                bl = ((br+bl)/2);
            }else{
                al = ((ar+al)/2);
                br = ((br+bl+1)/2);
            }
        }
        
        if (left==1){
            if (al+bl < a-ar+b-br-2){
                return (double) Math.max(Math.max(A[al],B[bl]),Math.min(A[ar],B[br]));
            }else {
                return (double) Math.min(Math.max(A[al],B[bl]),Math.min(A[ar],B[br]));
            }
        }else{
            return (double) 0.5*(Math.max(A[al],B[bl])+Math.min(A[ar],B[br]));
        }
    }

}
