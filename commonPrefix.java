
public class commonPrefix {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] test = new String[]{"acbd","acbbadgfadsg"};
		System.out.println(longestCommonPrefix(test));
		int[] digits = new int[]{1,0};
		int[] result = plusOne(digits);
		
	}
	
	public static String longestCommonPrefix(String[] strs) {
        if (strs.length==0) return new String();
        StringBuffer sb = new StringBuffer(strs[0]);
        for (String str: strs){
            for (int i=sb.length();i>0;i--){
                if (str.indexOf(sb.substring(0, i))>=0){
                    sb = new StringBuffer(sb.substring(0, i));
                    break;
                }
                
            }
        }
        return sb.toString();
    }
	public static int[] plusOne(int[] digits) {
        int carry = 1;
        for (int i=digits.length-1;i>=0;i--){
            int temp = digits[i];
            digits[i] = (temp+carry)%10;
            carry = (temp+carry)/10;
        }
        if (carry==1) {
            int[] result = new int[digits.length+1];
            result[0] = 1;
            for (int i = 1;i<result.length;i++){
                result[i] = digits[i-1];
            }
            return result;
        }
        return digits;
    }
}
