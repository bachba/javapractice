import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
public class GEMCloudCSVReader {
	public static void main(String[] args)throws IOException{
		BufferedReader br=null;
		FileReader fr =null;
		String rawline=null;
		String[] splitted;
		List<String[]> list = new ArrayList<String[]>();
		List<Integer> IDlist = new ArrayList<Integer>();
		int total=0;
		int wifi=0;
		String id = null;
		
		BufferedReader brID=null;
		try{
			brID = new BufferedReader(new FileReader("C:\\Users\\He\\Dropbox\\UCB\\GEMcloud Data\\QualifiedUserID.txt"));
			for (int i = 0;i<134;i++){
				IDlist.add(Integer.parseInt(brID.readLine()));
			}
		}finally{
			brID.close();
		}
		
		long starttime = System.currentTimeMillis();
		try{
			fr = new FileReader("C:\\Users\\He\\Dropbox\\UCB\\GEMcloud Data\\After Open to Public\\finished_jobs (032514).csv");
//			fr = new FileReader("C:\\Users\\He\\Dropbox\\UCB\\GEMcloud Data\\post-survey (only qualified participants).xlsx");
			br = new BufferedReader(fr);
			rawline = br.readLine();
			splitted=rawline.split(",");
			
			int counter=0;
			//skip the data before GEMCloud study open to public
			while (counter<16213){
				rawline = br.readLine();
				counter++;	
			}
			rawline = br.readLine();
			while (rawline!=null){
				
				splitted=rawline.split(",");
				list.add(splitted);
				
					

				rawline = br.readLine();
				
			}
			System.out.println("Time cost: "+(System.currentTimeMillis()-starttime));

			
		}
		finally{
			try
			{	
				br.close();
			}catch(Exception e){
				
			}
		}
		String[] dataentry;
		for (int i = 0;i<IDlist.size();i++){
			for(int j=0;j<list.size();j++){
				dataentry = list.get(j);

				String sid =dataentry[1].replaceAll("^\"|\"$", "");
				if(Integer.parseInt(sid)==IDlist.get(i)){
					if (dataentry[10].equalsIgnoreCase("\"WIFI\"")){
						wifi++;
					}
					total++;
				}
			}
		}
		System.out.println("Time cost: "+(System.currentTimeMillis()-starttime));
		System.out.println("total: "+total+"wifi= "+wifi);
			
	}
}
