

public class copylinkedlist {
    public static LinkedListNode copyRandomList(LinkedListNode head) {
        LinkedListNode headcopy = new LinkedListNode (head.label);
        LinkedListNode temp=head;
        LinkedListNode prev;
        
        prev = headcopy;
        while(temp.next!=null){
            
            prev.next = new LinkedListNode(temp.next.label);
            prev = prev.next;
            temp = temp.next;
        }
        
        return headcopy;
    }
    public static void main(String args[]) {
    	LinkedListNode head = new LinkedListNode(0);
    	LinkedListNode prev = head;
    	for (int i=1;i<5;i++){
    		LinkedListNode tail = new LinkedListNode(i);
    		prev.next = tail;
    		prev = tail;
    	}
    	LinkedListNode copy = copyRandomList(head);
    	System.out.println(copy.label);
    	while (copy.next!=null){
    		copy = copy.next;
    		System.out.println(copy.label);
    	}
    }
}

class LinkedListNode {
   int label;
   LinkedListNode next;
   LinkedListNode(int x) { this.label = x; next = null;}
}