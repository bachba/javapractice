
public class matrix {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] matrix = new int[][]{{1,3,1},{1,5,1},{4,2,1}};
		
		for (int i=0;i<matrix.length;i++){
			
			for (int j=0;j<matrix[0].length;j++){
				System.out.print(matrix[i][j]);
			}
			System.out.print("\n");
		}
		System.out.println(minPathSum(matrix)+"");
	}
	
	public static int minPathSum(int[][] grid) {
        if (grid==null ||grid[0]==null) return 0;
        if (grid[0].length==1) return grid[0][0];
        
        int[] cache = new int[grid[0].length];
        cache[0] = grid[0][0];
        for (int i=1;i<grid[0].length;i++) 
        {
            cache[i] = cache[i-1]+grid[0][i]; //the first "row" of sum value at each spot
        }
        
        for (int i=1;i<grid.length;i++){
            for (int j=0;j<grid[0].length;j++){
                if (j==0){
                    cache [j] = cache [j]+grid[i][j];
                }else{
                    cache[j] = Math.min(cache[j-1]+grid[i][j],cache[j]+grid[i][j]);
                }
            }
        }
        return cache[cache.length-1];
    }

}
