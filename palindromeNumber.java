
public class palindromeNumber {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isPalindrome(1410110141));
	}

	public static boolean isPalindrome(int x) {
        if (x<0) return false;
        int high =10;
        while (x/(high*10)>0) high*=10;
        if (x>1000000000) high = 1000000000;
        int low = 1;
        while (high>low){
        	int hd = (x/high)%10;
        	int ld = x%(low*10)/low;
            if (((x/high)%10)!=(x%(low*10)/low)) return false;
            high /=10;
            low *=10;
        }
        return true;
    }
}
