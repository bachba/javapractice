
public class fizzbuzz {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long start = System.currentTimeMillis();
		fizzbuzz2(1, (int)Math.pow(10, 7));
		System.out.println();
		System.out.println(""+(System.currentTimeMillis()-start));
	}
	static String fizzbuzz(int start, int end){
		StringBuilder sb = new StringBuilder();
		for (int i = start;i<=end;i++){
			if (i%15==0){
				sb.append(", FizzBuzz");
			}else if (i%5==0){
				sb.append(", Buzz");
			}else if (i%3==0){
				sb.append(", Fizz");
			}else {
				sb.append(", "+i);
			}
			
		}		
		return sb.toString().substring(2);
	}
	static String fizzbuzz2(int start, int end){
		String s ="";
		for (int i = start;i<=end;i++){
			if (i%15==0){
				s+=", FizzBuzz";
			}else if (i%5==0){
				s+=", Buzz";
			}else if (i%3==0){
				s+=", Fizz";
			}else {
				s+=", "+i;
			}
			
		}		
		return s.substring(2);
	}
}
