import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class angrychildren2 {
    
    static long difference (ArrayList<Integer> ar, int start,int K){
        long diff=0;
        diff += ((long)ar.get(start-1))*(K-1);
        for (int n= 0;n<K-1;n++){
            diff -= ((long) ar.get(start+n))*2;
        }
        diff += ((long) ar.get(start+K-1))*(K-1);
        return diff;
    }
    

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		int K = in.nextInt();
		ArrayList<Integer> ar = new ArrayList<Integer>();
		long fairness=0;
        long lastfairness = 0;
        long lowestfairness = 0;
		for (int i = 0;i<N;i++){
			ar.add(in.nextInt());
		}
		in.close();
        Collections.sort(ar);
        
        
        for (int i = 0;i<K;i++){
            fairness += (1-K+i*2)*(long)ar.get(i);
        }
        //fairness = fairness/2;
        lastfairness = fairness;
        lowestfairness = fairness;
        for (int i = 1;i<=N-K;i++){
            fairness = lastfairness + difference(ar,i,K);
//            System.out.println(fairness);
            if (fairness<lowestfairness){
            	lowestfairness=fairness ;
            }
            
        }
        lastfairness = fairness;
		
		
		
		System.out.println(lowestfairness);
    }
}