import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class taskscore_servertime {
	public static void main(String[] args)throws IOException{
		
		
		String[] SQLdatasplitted;
		String[] splitted;
		String[] seperateraw;
		Map<String,String> SQLmap = new HashMap<String,String>();
		Map<String,String> resultsTMmap = new HashMap<String,String>(); 
//		List<Long> resultslist = new ArrayList<Long>();
//		List<Float> TMlist = new ArrayList<Float>();
		BufferedReader brTM=null;
		
		long total_time_processed=0;
		long total_time_processedTM04=0;
		int total_tasks=0;
		int total_tasks_TM04 = 0;
		int total_tasks_check = 0;
		double TMscoretotal = 0;
		double TMscore04 = 0;
		int missed = 0;
		double TMscoreMax = 0;
		double TMscoreMin = 1;
		
		long starttime = System.currentTimeMillis();
		
		
		try{
			brTM = new BufferedReader(new FileReader("C:\\Users\\He\\Dropbox\\UCB\\GEMcloud Data\\SAINT2Results\\TM_task_results_4.txt"));
			String rawdata=brTM.readLine();
			while(rawdata!=null){
				total_tasks++;

				seperateraw=rawdata.split("\\s+");
				rawdata = brTM.readLine();
				
				splitted = seperateraw[0].split("_");
//				resultslist.add(Long.parseLong(splitted[2]));
//				TMlist.add(Float.parseFloat(seperateraw[1]));
				resultsTMmap.put(splitted[2],seperateraw[1]);//key: task sent time, value: TMscore
			}
		}finally{
			brTM.close();
		}
		
		try{
			brTM = new BufferedReader(new FileReader("C:\\Users\\He\\Dropbox\\UCB\\GEMcloud Data\\SAINT2Results\\finished_jobs (032714).txt"));
			String SQLrawdata=brTM.readLine();
			while(SQLrawdata!=null){
				total_tasks++;

				SQLdatasplitted=SQLrawdata.split("\\s+");
				long[] servertimes = new long[2];
				servertimes[0] = Long.parseLong(SQLdatasplitted[6]);//task sent time
				servertimes[1] = Long.parseLong(SQLdatasplitted[7]);//result recv time 
//				SQLmap.put(SQLdatasplitted[6],SQLdatasplitted[6]);//task sent time as key recv time as value
				
				if (resultsTMmap.get(SQLdatasplitted[6])==null){
					//System.out.println(senttime);
					missed++;
				}else{
					total_time_processed +=(servertimes[1]-servertimes[0]);//millisecond long <1e19
					double tmscore = Double.parseDouble(resultsTMmap.get(SQLdatasplitted[6]));
					if (tmscore > TMscoreMax) TMscoreMax = tmscore;
					if (tmscore < TMscoreMin) TMscoreMin = tmscore;
					if (tmscore>0.45){					
						TMscore04 += tmscore;
						total_time_processedTM04 +=(servertimes[1]-servertimes[0]);//millisecond
						total_tasks_TM04++;
					}
					TMscoretotal += tmscore;
					resultsTMmap.remove(SQLdatasplitted[6]);
					total_tasks_check++;
				}//get TMscore for the file
				SQLrawdata = brTM.readLine();
			}
		}finally{
			brTM.close();
		}
				
		System.out.println("Time cost: "+(System.currentTimeMillis()-starttime));

		System.out.println("Total Results: "+total_tasks_check);
		System.out.println("Total TM04: "+total_tasks_TM04);
		System.out.println("TM04 ratio: "+(double)total_tasks_TM04/(double)total_tasks_check);
		System.out.println("Average time: "+total_time_processed/total_tasks_check);

		System.out.println("Total time: "+total_time_processed);
		System.out.println("Average TMscore: "+TMscoretotal/total_tasks_check);
		System.out.println("Avarage time per TM04: "+total_time_processed/total_tasks_TM04);
		//including time processed for lower score results 
		System.out.println("Avarage score TM04: "+TMscore04/total_tasks_TM04);
		System.out.println("TMscoreMax: "+TMscoreMax+"TMscoreMin: "+TMscoreMin);
			
	}
}
