

/* Head ends here */
import java.util.*;
public class Solution {

	static void quickSort(int A[], int f, int l)
	{
		if (f > l){ 

			return;}


		int pivot_index = partition2(A, f, l);
		//      printsubarray(A,f,l);
		//      System.out.print("pivot_index = "+pivot_index);

		//      printsubarray(A,f,pivot_index-1);
		quickSort(A, f, pivot_index-1);

		//      printsubarray(A,pivot_index+1,l);
		quickSort(A, pivot_index+1, l);

		printsubarray(A,f,l);
	}   
	static void printsubarray(int A[],int left,int right){
		if(right-left<1)
			return;
		for (int i=left; i<=right;i++){
			System.out.print(A[i]+" ");
		}
		System.out.println("");
	}

	//          static void quickSort(int A[]){
	//              int pivot_index = partition(A, f, l);
	//        	  int[] left = new int[pivot_index-f];
	//              for(int i = f;i<=l;i++){
	//            	  left[i-f] = A[i];
	//              }
	//              int[] right = new int[l-pivot_index-1];
	//              for(int i = pivot_index+1;i<l;i++){
	//            	  right[i-pivot_index-1] = A[i];
	//              }
	//
	//              quickSort(left);
	//              quickSort(right);
	//          }
	static void swap (int A[], int x, int y)
	{
		int temp = A[x];
		A[x] = A[y];
		A[y] = temp;
	}

	static int partition2 (int A[],int f, int l){
		int pivot = f;
		if (f<l){

			for (int i = f+1;i<=l;i++){
				if (A[i]<=A[pivot]){
					for (int j =i;j>pivot;j--){
						swap(A,j,j-1);

					}
					pivot++;
					//    			printArray(A);
				}
			}
		}
		return pivot;
	}
	static int partition(int A[], int f, int l)
	{
		int pivot = A[f];
		while (f < l)
		{

			while (A[f] < pivot) f++;
			while (A[l] > pivot) l--;
			swap (A, f, l);

		}
		return f;
	}

	/* Tail starts here */

	static void printArray(int[] ar) {
		for(int n: ar){
			System.out.print(n+" ");
		}
		System.out.println("");
	}

	public static void main(String[] args) {
		//           Scanner in = new Scanner(System.in);
		//           int n = in.nextInt();
		//           int[] ar = new int[n];
		//           for(int i=0;i<n;i++){
		//              ar[i]=in.nextInt(); 
		//           }
		//           in.close();
		int [] ar = {5,8,1,3,7,9,2};
		quickSort(ar,0,ar.length-1);
		//printArray(ar);

	}    
}
