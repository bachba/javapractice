import java.util.ArrayList;
import java.util.List;



public class restoreIP {
	
	public static List<String> restoreIpAddresses(String s) {
	    List<String> results = new ArrayList<String>();
	    findIP(s,0,0,"",results);
	    return results;
	}
	static void findIP(String s,int start, int count,String temp, List<String> r){
	    if(start==s.length()&&count==4) {
	        r.add(temp);
	        return;
	    }else if(temp.length()>15||count>4){
	        return;
	    }
	    for (int i = start+1;i<=Math.min(s.length(),start+3);i++){
	        String part = s.substring(start,i);
	        if (Integer.parseInt(part)<256){
	            if (part.charAt(0)=='0'&&i-start>1)return;
	            String temp2;
	            if (start==0){
	                temp2 = part;
	            }else{
	                temp2 = temp + "."+part;
	            }
	            findIP(s,i,count+1,temp2,r);
	        }
	    }
	}
	
	public static void main(String[] args){
		System.out.println(restoreIpAddresses("010010"));
	}
}