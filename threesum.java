import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;


public class threesum {

    public static ArrayList<ArrayList<Integer>> threeSum(int[] num) {
        Arrays.sort(num);
        ArrayList<ArrayList<Integer>> results= new ArrayList<ArrayList<Integer>>();
        for (int i =0; i<num.length;i++){
        	if (num[i]>0)continue;
            if (i>0&&num[i]==num[i-1])continue;
            twosum(num,i,results);
        }
        return results;
    }
    public static void twosum(int[] num, int k, ArrayList<ArrayList<Integer>> out){
    	Map<Integer,Integer> cache = new HashMap<Integer,Integer>();
        HashSet<Integer> checked = new HashSet<Integer>();
        for (int i=k+1;i<num.length;i++){
            if (checked.contains(num[i]))continue;
            if (cache.containsKey(num[i])){
                if (num[k]>0-num[k]-num[i])return;
                ArrayList<Integer> entry = new ArrayList<Integer>();
                entry.add(num[k]);
                entry.add(0-num[k]-num[i]);
                entry.add(num[i]);
                out.add(entry);
                checked.add(num[i]);
            }else{
            	cache.put(0-num[k]-num[i], num[i]);
            }
        }
        
    }
	public static void main(String[] args) {
		int[] num = new int[] {1,-1,0,-2,2};
		ArrayList<ArrayList<Integer>> a = threeSum(num);
		System.out.println(a);
	}

}
