import java.util.LinkedList;


public class linkedlist {
	
	
	
	
	public static void main(String args[]){
		Node head = new Node("a",null,null);
		Node head2 = new Node("b",head,null);
//		LinkedList<String> ll = new LinkedList<String>();
//		ll.add("a");
//		ll.add("test");
		
		System.out.println("head"+head2);
	}

}

class Stack {
	private Node top;
	private int size;
	
	Stack(){
		top = null;
		size = 0;
	}
	
	public int size(){return size;}
	public boolean isEmpty(){return size==0;}
	public void push(Object o){
		Node a = new Node(o,null,top);
		top = a;
		size++;
	}
	public Object pop() { //why use object instead of Node
		size--;
		Object temp = top;
		top= top.getNext();
		return temp;
	}
	
}

class Node{
	private Object element;
	private Node next,previous;
	
	
	Node(){
		this(null,null,null);
	}
	
	public Node(Object e, Node p, Node n){
		element = e;
		next = n;
		previous = p;
	}
	
	void setElement(Object newE){	element = newE;	}
	void setNext(Node newN){	next = newN;	}
	void setPrev(Node newP){	previous = newP;	}
	
	Object getElement(){ return element;}
	Node getNext(){ return next;}
	Node getPrev(){ return previous;}
}
