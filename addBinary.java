import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class addBinary {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println("".length());
		//System.out.println(addBinary("1010","1011"));
		
		List<List<Integer>> a = new ArrayList<List<Integer>>();
		a.add(new ArrayList<Integer>(Arrays.asList(1)));
		a.add(new ArrayList<Integer>(Arrays.asList(2,3)));
		System.out.print(minimumTotal(a));
	}
	
	public static int minimumTotal(List<List<Integer>> triangle) {
		int tn = triangle.size();
        if (tn==0) return 0;
        if (tn==1) return triangle.get(0).get(0);
        ArrayList<Integer> a = (ArrayList<Integer>) triangle.get(tn-1);
        Integer[] n = a.toArray(new Integer[tn]);
        for (int i= tn-2; i>=0;i--){
            a = (ArrayList<Integer>) triangle.get(i);
            Integer [] temp = a.toArray(new Integer[i+1]);
            for (int j = 0; j<= i;j++){
                temp[j] = Math.min(n[j]+temp[j],n[j+1]+temp[j]);
            }
            n = temp;
        }
        return n[0];
    }
	
	public static String addBinary(String a, String b) {
        int overlap = Math.min(a.length(),b.length());
        char carry = '0';
        StringBuffer sb = new StringBuffer();
        for (int i=1;i<=overlap;i++){
            char aa = a.charAt(a.length()-i);
            char bb = b.charAt(b.length()-i);
            if (aa==bb){
                if (carry=='1'){
                        sb.append('1');
                }else{
                        sb.append('0');
                }
                if (aa=='1'){
                    carry = '1';
                }else{
                    carry = '0';
                }
            }else{
                if (carry=='1'){
                    sb.append('0');
                    carry = '1';
                }else{
                    sb.append('1');
                    carry = '0';
                }
            }
        }
        if (a.length()>b.length()){
            for (int i = overlap+1;i<=a.length();i++){
                char aa = a.charAt(a.length()-i);
                if (aa=='1'){
                    if (carry =='1'){
                        sb.append('0');
                        carry = '1';
                    }else{
                        sb.append('1');
                        carry = '0';
                    }
                }else{
                    if (carry=='1'){
                        sb.append('1');
                        carry = '0';
                    }else{
                        sb.append('0');
                        carry = '0';
                    }
                }
            }
            
        }
        if (a.length()<b.length()){
            for (int i = overlap+1;i<=b.length();i++){
                char bb = b.charAt(b.length()-i);
                if (bb =='1'){
                    if (carry =='1'){
                        sb.append('0');
                        carry = '1';
                    }else{
                        sb.append('1');
                        carry = '0';
                    }
                }else{
                    if (carry=='1'){
                        sb.append('1');
                        carry = '0';
                    }else{
                        sb.append('0');
                        carry = '0';
                    }
                }
            }
            
        }
        if (carry=='1') sb.append('1');
        return sb.reverse().toString();
    }

}
