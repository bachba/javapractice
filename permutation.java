import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack; 


public class permutation{

	public static void main(String args[]){
		ArrayList<ArrayList<Integer>> test = new ArrayList<ArrayList<Integer>>();
		int [] num = {1,2,3,4};
		
		Arrays.sort(num);
		boolean[] used = new boolean[num.length];
		ArrayList<Integer> answer = new ArrayList<Integer>();
		ArrayList<Integer> nextp = new ArrayList<Integer>();
		for (int i =0;i<num.length;i++){
			used[i]=false;
			answer.add(num[i]);
			nextp.add(num[i]);
		}
		Stack s = new Stack();
		for (int i=0;i<num.length;i++){
			s.push(num[i]);
		}
		for (int i=0;i<num.length;i++){
			num[i] = (int) s.pop();
		}
		//permute(num,1,used,answer,test);
		//System.out.println(answer);
		
		test.add(nextp);
		boolean loop=true;
		while (true){
			
			nextp =nextPermute(answer);
			if (nextp !=null){
			test.add(nextp);
			}else{
				break;
			}
		}

		System.out.println("#: "+test.size()+test);
	}
	
	public static ArrayList<Integer> nextPermute(ArrayList<Integer> nextp){
		ArrayList<Integer> nextperm= new ArrayList<Integer>();
		//nextperm = (ArrayList<Integer>) perm.clone();
//		for (Integer ii : nextperm){
//			nextp.add(new Integer(ii));
//		}
		
		int k=-1,l=0;
		for (int i = 0;i<nextp.size()-1;i++){
			if (nextp.get(i)<nextp.get(i+1))k=i;
		}
		if (k==-1)return null;
		for (int i = k;i<nextp.size()-1;i++){
			if (nextp.get(k)<nextp.get(i+1))l=i+1;
		}
		Integer temp = nextp.get(k);
		nextp.set(k, nextp.get(l));
		nextp.set(l,temp);
		Stack<Integer> stack = new Stack<Integer>(); 
//		for (int i = k+1;i<nextp.size();i++){
//			stack.push(nextp.get(i));
//		}
//		for (int i = k+1;i<nextp.size();i++){
//			nextp.set(i,stack.pop());
//		}
		for (int i = k+1;i<(nextp.size()+k+1)/2;i++){//use xor to reverse int array
			nextp.set(i, nextp.get(i)^ nextp.get(nextp.size()-i+k));
			nextp.set(nextp.size()-i+k,nextp.get(i)^ nextp.get(nextp.size()-i+k));
			nextp.set(i, nextp.get(i)^ nextp.get(nextp.size()-i+k));
		}
		
		
		for (Integer ii : nextp){
			nextperm.add(new Integer(ii));
		}
		
		
		
		return nextperm;
	}
	
	public static void permute (int[] num, int index,boolean[]used, ArrayList<Integer> answer,ArrayList<ArrayList<Integer>> array){
		if (answer.size()==num.length){
			array.add((ArrayList<Integer>)answer.clone());
			return;
		}
		for (int i=0;i<num.length;i++){
			if (used[i]==true) continue;
			answer.add(num[i]);
			used[i]=true;
			permute(num,++index,used,answer,array);
			used[i]=false;
			answer.remove(new Integer(num[i]));
			
		}
	}
}
