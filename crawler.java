import java.util.*;
import java.net.*;
import java.io.*;


public class crawler {

	public static LinkedList<String> links;
	public static String host;
	public static int rate;
	public static int pages;
	public static int bytes;
	public static ArrayList<String> files;
	public static void main(String args[]) throws Exception{
		//get the target host
		host = args[0];
		rate = Integer.parseInt(args[1]);
		//create the folder to store the html files
		new File(host+"/").mkdir();
		//an arraylist to store the already visited pages
		files = new ArrayList<String>();
		//this is the queue to tempararily store new links 
		links = new LinkedList<String>();
		links.offer(host);
		long last = System.currentTimeMillis();
		//byte / sec
		if(rate>20){
			while(true){
				long current = System.currentTimeMillis();
				if(current-last>1000){
					bytes -= rate;
					if(bytes<0)
						bytes = 0;
					last = current;
				}
				if(links.size()!=0 && bytes<rate){
					String link = links.poll();
					String result = processURL(link);
				}
			}
		}
		//page/sec
		else{
			while(true){
				long current = System.currentTimeMillis();
				if(current-last>1000){
					pages -= rate;
					if(pages<0)
						pages = 0;
					last = current;
				}
				if(links.size()!=0  && pages<rate){
					String link = links.poll();
					String result = processURL(link);
				}
			}
		}

	}
	public static String processURL(String URL) throws Exception{
		//strip out http://
		if(URL.startsWith("http://")){
			URL = URL.replaceFirst("http://","");
		}
		if(URL.startsWith("http//")){
			URL = URL.replaceFirst("http//","");
		}
		//not support https://
		else if(URL.startsWith("https://")){
			System.out.println("https not supported");
			return "";
		}
		//split to host and the location of file
		int firstSlash = URL.indexOf('/');
		//add the slash if the url doesn't have it
		if(firstSlash==-1){
			URL = URL+"/";
			firstSlash = URL.length()-1;
		}
		int slash = URL.lastIndexOf('/');
		int dot = URL.lastIndexOf('.');
		//the link fogot the last slash
		if(dot<slash && URL.charAt(URL.length()-1)!='/')
			URL = URL + "/";
		return request(URL.substring(firstSlash));
	}
	public static String request(String dir) throws Exception{
		//update the list of visited files
		if(files.contains(dir))
			return "";
		else
			files.add(dir);
		Socket s = null;
		PrintWriter out = null;
		BufferedReader in = null;
		//TCP connecto to the server on port 80
		try {
			s = new Socket(host, 80);
			out = new PrintWriter(s.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Don't know about host");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection");
			System.exit(1);
		}
		//HTTP request
		System.out.println("Sending requst to "+host+dir);
		out.println("GET "+dir+" HTTP/1.1");
		out.println("Host: "+host);
		out.println("Connection: close");
		out.println();
		//get the response status
		String temp = in.readLine();
		System.out.println(temp);
		if(!temp.equals("HTTP/1.1 200 OK")){
			System.out.println("Failed");
			System.out.println();
			return "";
		}
		//update number of pages
		pages++;
		System.out.println("page number: "+pages);
		//get the headers
		String extension = "";
		while((temp=in.readLine())!=null){
			if(temp.length()==0){
				break;
			}
			//System.out.println(temp.length());
			String[] tokens = temp.split(": ");
			if(tokens[0].equals("Content-Type")){
				System.out.println(temp);
				extension = "."+tokens[1].split("/")[1].split(";")[0];
			}
			else if(tokens[0].equals("Content-Length")){
				System.out.println(temp);
				bytes += Integer.parseInt(tokens[1]);
				System.out.println("Bytes number: "+bytes);
			}
		}
		//contruct the filename
		//cases only have directory , without filename
		String filename = "";
		int slash = dir.lastIndexOf('/');
		if(slash==dir.length()-1){
			filename = "index"+extension;
		}
		else{
			filename = dir.substring(slash+1);
		}
		if(!filename.endsWith(extension)){
			filename+=extension;
		}
		dir = host+dir.substring(0,slash)+"/";
		//get the file content
		StringBuffer stream = new StringBuffer();
		while((temp=in.readLine())!=null){
			if(extension.startsWith(".html"))
				temp = findLink(temp,dir);
			stream.append(temp);
		}
		//save to local
		save(stream.toString(),dir,filename);
		s.close();
		in.close();
		out.close();
		return dir+":::"+filename;
	}
	public static void save(String s, String dir, String filename) throws Exception{
		try{
			System.out.println("Saving "+filename+" to "+dir+"....");
			File d = new File(dir);
			if(!d.exists())
				d.mkdirs();
			File file = new File(dir+filename);
			BufferedWriter output = new BufferedWriter(new FileWriter(file));
			output.write(s);
			output.close();
			System.out.println("Success!");
			System.out.println();
			return;
		}
		catch(IOException e){
			System.out.println("writing file error");
			return;
		}
	}
	public static String findLink(String tag,String dir){
		int l = 0;
		while((l=tag.indexOf("href",l))>=0){
			int start = tag.indexOf('"',l);
			int end = tag.indexOf('"',start+1);
			if(start-l>10 || end < 0 || start<0){
				start = tag.indexOf('\'',l);
				end = tag.indexOf('\'',start+1);
			}
			if(start-l>10 || end < 0 || start<0){
				l+=10;
				continue;
			}
			String link = tag.substring(start+1,end);
			if(link.startsWith("http://") && link.contains(host)){
				//System.out.println("new link added: "+link);
				links.offer(link);
			}
			else if(link.startsWith("https")){
				;
			}
			else if(!link.startsWith("http://") && !link.startsWith("#") && !link.startsWith("data:")){
				//System.out.println("new link added: "+dir+link);
				if(link.startsWith("/")){
					tag = tag.substring(0,start+1)+tag.substring(start+2);//replaceFirst(link,link.substring(1));
					links.offer(dir+link.substring(1));
				}
				else{
					links.offer(dir+link);
				}
			}
			l = end+1;
		}
		l = 0;
		while((l=tag.indexOf("src",l))>=0){
			int start = tag.indexOf('"',l);
			int end = tag.indexOf('"',start+1);
			if(start-l>10 || end < 0 || start<0){
				start = tag.indexOf('\'',l);
				end = tag.indexOf('\'',start+1);
			}
			if(start-l>10 || end < 0 || start<0){
				l+=10;
				continue;
			}
			String link = tag.substring(start+1,end);
			if(link.startsWith("http://") && link.contains(host)){
				//System.out.println("new link added: "+link);
				links.offer(link);
			}
			else if(link.startsWith("https")){
				;
			}
			else if(!link.startsWith("http://") && !link.startsWith("#") && !link.startsWith("data:") && !link.contains("javascript")){
				//System.out.println("new link added: "+dir+link);
				if(link.startsWith("/")){
					tag = tag.substring(0,start+1)+tag.substring(start+2);//replaceFirst(link,link.substring(1));
					tag = tag.replaceFirst(link,link.substring(1));
					links.offer(dir+link.substring(1));
				}
				else{
					links.offer(dir+link);
				}
			}
			l = end+1;
		}
		return tag;
	}
}


