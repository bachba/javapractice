import java.util.HashMap;
import java.util.Map;


public class uniqueBST {

	
	    static long total;
	    static Map<Integer,Long> cache;
	    public static long numTrees(int n) {
	        total = 0;
	        cache = new HashMap<Integer,Long>();
	        int[] num =  new int[n];
	        for (int i=1;i<=n;i++){
	            num[i-1]=i;
	        }
	        for (int i=0;i<n;i++){
	        total += buildBST(0,n-1,i);
	        }
	        
	        
//	        buildBST(num,new int[0],0);
	        return total;
	    }
	    public static long buildBST(int leftstart,int rightend, int rootindex){
	    	  	
	    	long lc=0;
	    	long rc=0;

	    	
	    	if (leftstart==rootindex){
	    		lc=1;
	    	}else if (cache.containsKey(rootindex-leftstart)){
	    		lc=cache.get(rootindex-leftstart);
	    	}else{
	    	//left child tree, select child tree root from leftstart to rootindex-1
	    			for (int i=0;i<rootindex;i++){
	    				lc+=buildBST(leftstart,rootindex-1,i);
	    			}
		    		cache.put(rootindex-leftstart,lc);
	    	}

	    	if (rightend==rootindex){
	    		rc=1;
	    	}else if (cache.containsKey(rightend-rootindex)){
	    		rc=cache.get(rightend-rootindex);
	    	}else{
	    		for (int i=rootindex+1;i<=rightend;i++){
	    			rc+=buildBST(rootindex+1,rightend,i);
	    		}
	    		cache.put(rightend-rootindex,rc);
	    	}
	    	
	    	return lc*rc;
	    }
	    
	    
//	    public static void buildBST(int[] small,int[] large,int root){
//	        if (small.length==1&&large.length==0){
//	            total+=1;
//	            return;
//	        }
//	        if (large.length==1&&small.length==0){
//	            total+=1;
//	            return;
//	        }
//	        for (int i = 0;i<small.length;i++){
//	            int[] newsmall = new int[i];
//	            System.arraycopy(small, 0, newsmall, 0, i);
//	            int[] newlarge = new int[small.length-i-1+large.length];
//	            System.arraycopy(small, i+1, newlarge, 0,small.length-i-1);
//	            if (large.length!=0) System.arraycopy(large, 0, newlarge, small.length-i-1,large.length);
//	            buildBST(newsmall,newlarge,small[i]);
//	        }
//	        for (int i = 0;i<large.length;i++){
//	            int[] newlarge2 = new int[large.length-i-1];
//	            System.arraycopy(large, i+1, newlarge2, 0,large.length-i-1);
//	        	int[] newsmall2 = new int[small.length+i];
//	            System.arraycopy(small, 0, newsmall2, 0,small.length);
//	            if (small.length!=0) System.arraycopy(large, 0, newsmall2, small.length-i-1,large.length);
//	            buildBST(newsmall2,newlarge2,large[i]);
//	        }
//	    
//	    }
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		long numberoftrees =  numTrees(7);
		HashMap<Integer,Integer> hm = new HashMap<Integer,Integer>();
        System.out.print(""+viableStructure(7, hm));
		long duration = (System.currentTimeMillis()-start);
		System.out.println(numberoftrees+"time: "+duration);
		
	}
	
	public static int viableStructure(int nodes, HashMap<Integer,Integer> hm){
        if (nodes==0){
            return 1;
        }
        if (nodes==1){
            return 1;
        }
        if (hm.containsKey(nodes)){
            return hm.get(nodes);
        }
        int sum=0;
        for (int i = 0; i <= nodes-1; i++){
             sum += viableStructure(nodes-i-1,hm)*viableStructure(i,hm); //multiplication of left and right
        }
        if (!hm.containsKey(nodes)){
            hm.put(nodes,sum);
        }
        return sum;
    }

}
